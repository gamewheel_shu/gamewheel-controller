var Gamewheel   = require('../gamewheel');
var helper      = Gamewheel.helper;

module.exports = function (_name) {

    var name        = helper.snake_case(_name);
    var path        = './' + name;
    var Controller  = require(path);

    var controller  = new Controller();

    return function (_method, _determine){


        if(!helper.isFunction(controller[_method])){
            throw 'controller method ' + _name + '.' + _method + '() not found';
        }

        if(_determine){
            return function (req, res, next){

                var result = controller[_method](req, res, function (brake) {
                    if (next && brake) { next(brake); }
                    else if (next)     { next({done: true}); }
                });
                return result;
            }
        }
        else {
            return controller[_method].bind(controller);
        }
    };
};

module.exports.catchBrake = function (brake, req, res, next) {
    if ( brake && brake.done ) { return next(); }
    throw brake;
};

module.exports.catchJump = function (brake, req, res, next) {
    if ( brake && brake.jump ) { return next(); }
    throw brake;
};

module.exports.catchError = function (error, req, res, next) {

    var error = {
        result  : error.result  || 'failure',
        status  : error.status  || 0,
        message : error.message || error,
        stack   : error.stack   || null,
        code    : error.code    || 100,
    };

    if (error.status == 6 && error.validationFailures) {
        error.validationFailures = error.validationFailures
    }

    res.status(400);
    res.contentType('application/json');
    return res.json(error);
};

module.exports.clean = function (_keys, _determine) {

    var keys = helper.isArray(_keys)
        ? _keys
        : helper.isString([_keys])
            ? [_keys]
            : [];

    return function (req, res, next) {

        keys.forEach(function (_key) {
           delete res.data[_key];
        });

        if(next && _determine) { next({done: true}); }
        else if(next) { next(); }

        return res.data;
    };
};

module.exports.hostIsNot = function (_hostList) {
    
    return function(req, res, next){
        var pass = _hostList.every(function(_host){
            return !_host.test(req.data.source.host);
        });

        return pass
            ? next()
            : next({ jump : true });
    };
}

module.exports.hostIs = function (_hostList) {
    
    return function(req, res, next){
        var pass = _hostList.some(function(_host){
            return _host.test(req.data.source.host);
        });

        return pass
            ? next()
            : next({ jump : true });
    };
}

