var Gamewheel           = require('../gamewheel');
var config              = Gamewheel.config;
var helper              = Gamewheel.helper;

/**
 *
 * @type {WizardController}
 */
module.exports = exports = WizardController;

/**
 *
 * @constructor
 */
function WizardController (){

    /** Models **/
    this.Wizard   = function () {
        return Gamewheel.Model.get('Wizard');
    };
}

WizardController.prototype.identifier = function (req, res, next, id) {
    req.data.wizard    = req.data.wizard || {};
    req.data.wizard.id = id;
    next && next();
    return req.data.wizard;
};


WizardController.prototype.get = function (req, res, next) {

    var account         = res.data.account;
    var user            = res.data.user;
    var accountWizard   = this.Wizard();
    var userWizard      = this.Wizard();
    var wizard          = this.Wizard();

    wizard.load({
        kind : 'gamewheel'
    });

    if(account && !DataComponent.isCollection(account)){
        accountWizard.findById(account.data.wizardIds);
        wizard.collection.push.apply(wizard.collection, accountWizard.collection);
    }

    if(user && !DataComponent.isCollection(user)){
        userWizard.findById(user.data.wizardIds);
        wizard.collection.push.apply(wizard.collection, userWizard.collection);
    }

    res.data.wizard = wizard;
    next && next();
    return wizard;
};