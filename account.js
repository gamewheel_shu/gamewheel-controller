var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/** @constructor */
function AccountController (){

    /** Models **/
    this.Account        = function () {
        return Gamewheel.Model('Account');
    };
}

AccountController.prototype.identifier = function (req, res, next, id) {
        req.data.account = req.data.account || {};
        req.data.account.id = id;
        next && next();
        return req.data.account;
};

AccountController.prototype.get = function (req, res, next) {

    // collect requirement
    var user        = res.data.user     || null;
    var account     = res.data.account  || null;
    var collection  = [];

    var error;

    if(req.data.account && req.data.account.id){

        // load the request by ID
        account = this.Account().findById(req.data.account.id);

        // singel instance
        if(account.isNotCollection() && !account.isLoaded()){
            throw {
                message : '',
                reason  : account.error,
                level   : 'error'
            };
        }
        else if(account.isNotCollection() && user && !account.data.checkUser(user)){
            throw {
                message : 'permission deny : get Account <-> User'
            };
        }
        // multible instance
        else if(account.isCollection() && user){

            collection = account.collection.reduce(function (_collection, _account) {
                if(_account.data.checkUser(user)){
                    _collection.push(_account);
                }
                return _collection;
            }, []);

            account.collection = collection;
        }
    }

    else if(!account && user){
        account = user.data.accounts;
    }

    else if(!account){
        account = this.Account().find();
    }

    if(account) {
        res.data.account = account;
    }

    next && next();
    return account;
};

AccountController.prototype.new = function(req, res, next){

    var user = res.data.user || null;
    var account, error;
    var assetLibrary = res.data.assetLibrary || null;

    if(!user){
        throw { message : 'connection error : new Account <-> User' };
    }

    var accountData = {
        name            : 'Account : ' + user.data.email,
        owner           : user.data.id,
        status          : this.Account().STATUS_ENABLED
    };

    if(assetLibrary){
        accountData.assetLibrary = assetLibrary;
    }

    account = this.Account().init(accountData).save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.clone = function (req, res, next) {

    var user    = res.data.user     || null;
    var account = res.data.account  || null;

    var clone;

    if(!account || account.isCollection()){
        throw {
            message : 'data missing : clone Account -> account'
        };
    }

    if(user && user.isCollection()){
        throw {
            message : 'type error : clone Account -> user is a collection'
        };   
    }

    if(user && !account.checkUser(user)){
        throw {
            message : 'permission deny : clone Account <-> User'
        };
    }

    clone = this.Account().clone(account);

    clone.data.name             = clone.data.name + '(copy)';
    clone.data.packageIds       = [];
    clone.data.roleIds          = [];
    clone.data.memberIds        = [];
    clone.data.pageIds          = [];
    clone.data.contentIds       = [];
    clone.data.campaignIds      = [];
    clone.save();

    /* check on success */
    if (!clone.isSaved()) {
        throw clone.error;
    }

    res.data.account = clone;
    next && next();
    return clone;
};

AccountController.prototype.save = function(req, res, next) {

    var account = res.data.account || null;

    /* requirements */
    if(!account){
        throw { message : 'data missing : clone Account -> account' };
    }

    if(req.data.account){

        if(account.isCollection()){

            account.collection = account.collection.map(function (_account) {

                _account = applyValue(req.data.account, _account).save();

                /* check on success */
                if (!_account.isSaved()) {
                    throw _account.error;
                }

                return _account;
            });
        }

        else {
            account = applyValue(req.data.account, account).save();

            /* check on success */
            if (!account.isSaved()) {
                throw account.error;
            }
        }


    }

    res.data.account = account;
    next && next();
    return account;

    /** functions **/

    function applyValue (_source, _target){

        /* loops thru all data elements */
        Object.keys(_source).forEach( function (_key) {

            if(helper.in(['id', '_id'], _key)) { return; }

            var isTargetValue = helper.isDefined(_target.data._doc[_key]);
            var hasChanged = _target.data[_key] !== _source[_key];

            if(isTargetValue && hasChanged){
                _target.data[_key] = _source[_key];
            }
        });

        return _target;
    }
};


/** add relationship */
AccountController.prototype.addCampaign = function(req, res, next){
    
    var account = res.data.account || null;
    var campaign = res.data.campaign || null;

    if(!account || account.isCollection()){
        throw { message : 'Account addCampaign : Account is missing'};
    }

    if(!campaign){
        throw { message : 'Account addCampaign : Campaign is missing'};
    }

    account.data.campaignIds.push.apply(account.data.campaignIds, campaign.getId(true));
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.addRole = function(req, res, next){
    
    var account = res.data.account || null;
    var role = res.data.role || null;

    if(!account || account.isCollection()){
        throw { message : 'Account addRole : Account is missing'};
    }

    if(!role){
        throw { message : 'Account addRole : Role is missing'};
    }
    
    account.data.roleIds.push.apply(account.data.roleIds, role.getId(true));
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.addContent = function(req, res, next){
    
    var account = res.data.account || null;
    var content = res.data.content || null;

    if(!account || account.isCollection()){
        throw { message : 'Account addContent : Account is missing'};
    }

    if(!content){
        throw { message : 'Account addContent : Content is missing'};
    }

    account.data.contentIds.push.apply(account.data.contentIds, content.getId(true));
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.addPage = function(req, res, next){
    
    var account = res.data.account || null;
    var page = res.data.page || null;

    if(!account || account.isCollection()){
        throw { message : 'Account addPage : Account is missing'};
    }

    if(!page){
        throw { message : 'Account addPage : Page is missing'};
    }

    account.data.pageIds.push.apply(account.data.pageIds, page.getId(true));
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.addMembership = function(req, res, next){
    
    var account = res.data.account || null;
    var membership = res.data.membership || null;

    if(!account || account.isCollection()){
        throw { message : 'Account addMembership : Account is missing'};
    }

    if(!membership){
        throw { message : 'Account addMembership : Membership is missing'};
    }

    account.data.membershipIds.push.apply(account.data.membershipIds, membership.getId(true));
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};


/** account specific controller methods **/
AccountController.prototype.enable = function(req, res, next) {

    var account = res.data.account || null;

    if(!account || account.isCollection()){
        throw { message : 'account : id is missing' };
    }

    account.data.status = Account.STATUS_ENABLED;
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.disable = function(req, res, next) {

    var account = res.data.account || null;

    if(!account || account.isCollection()){
        throw { message : 'account : id is missing' };
    }

    account.data.status = Account.STATUS_DISABLED;
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};

AccountController.prototype.close = function(req, res, next) {

    var account = res.data.account || null;
    
    if(!account || account.isCollection()){
        throw { message : 'account : id is missing' };
    }

    account.data.status = Account.STATUS_CLOSED;
    account.save();

    /* check on success */
    if (!account.isSaved()) {
        throw account.error;
    }

    res.data.account = account;
    next && next();
    return account;
};


/** @type {AccountController} */
module.exports = AccountController;