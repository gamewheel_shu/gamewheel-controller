var fs                  = require('file-system');
var mkpath              = require('mkpath');
var imageSize           = require('image-size');
var deasync             = require('deasync');

var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 * builds the assets paths
 * @return {[type]} [description]
 */
var buildAssetPath = function(_asset){
    
    var extension = 'gw.data';
    switch (_asset.data.contentType) {
        case 'image/jpeg'       :
        case 'image/jpg'        : extension = 'jpg';    break;
        case 'image/png'        : extension = 'png';    break;
        case 'image/gif'        : extension = 'gif';    break;
        case 'image/bmp'        : extension = 'bmp';    break;
        case 'application/json' : extension = 'json';   break;
    }

    var assetFolder = [process.cwd(), '..' , config.api.uploadPath, 'account-'+_asset.data.accountId].join('/');
    var assetFile   = [_asset.data.id.toString(), extension].join('.');

    return {
        folder      : assetFolder,
        file        : assetFile,
        extension   : extension,
        path        : [assetFolder, assetFile].join('/')
    }
};


/**
 *
 * @constructor
 */
function AssetController () {

    this.Asset   = function (){
        return Gamewheel.Model.get('Asset');
    };
}

/**
 * transfers the asset identifier to the request object
 * @return {Object}        asset request object
 */
AssetController.prototype.identifier = function(req, res, next, id){
    req.data.asset       = req.data.asset || {};
    req.data.asset.id    = id;
    next && next();
    return req.data.asset;
};



/**
 * Gets an Asset
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
AssetController.prototype.get = function (req, res, next) {
    
    var account     = res.data.account || null;
    var asset;

    if(account && account.isCollection()){
        throw {
            message : 'Asset.get() : AccountCollection',
            level   : 'error',
            reason  : 'TypeError'
        }
    }

    if(req.data.asset && req.data.asset.id){

        asset = this.Asset().findById(req.data.asset.id);
        if(asset.isNotCollection()){
            if(account && !helper.in(account.data.assetLibrary.data.assetIds, asset.data.id.toString())){
                throw {
                    message : 'Asset.get() : Asset <-> Account',
                    reason  : 'RelationshipError',
                    level   : 'error'
                };
            }
        }
    }

    if(!asset){
        throw {
            message : 'Asset.get() : asset is missing',
            reason  : 'TypeError',
            level   : 'error'
        }
    }

    if(req.data.asset.loadFromFile){
        asset.perform(function (_asset) {
        
            var assetPath = buildAssetPath(_asset);
            var fileData = fs.readFileSync(assetPath.path, {
                encoding : _asset.data.encoding
            });

            try{
                _asset.data.payload = (_asset.data.contentType === 'application/json')
                    ? JSON.parse(fileData)
                    : 'data:'+asset.data.contentType+';'+_asset.data.encoding+','+fileData;
            }
            catch(_err){
                _asset.data.payload = {};
            }
        });    
    }
    

    res.data.asset = asset;
    next && next();
    return asset;
};

/**
 *
 * @param req
 * @param res
 * @param next
 */
AssetController.prototype.new = function (req, res, next) {

    var asset   = res.data.asset    || null;
    var account = res.data.account  || null;

    if(!account || account.isCollection()){
        throw {
            message : 'Asset.new() : account is required',
            reason : 'TypeError',
            level : 'error'
        };
    }

    asset = this.Asset().init({
        account     : account
    });

    res.data.asset = asset;
    next && next();
    return asset;
};

AssetController.prototype.save = function (req, res, next) {

    var asset   = res.data.asset    || null;
    var account = res.data.account  || null;

    if(!asset || asset.isCollection()){
        throw {
            message : 'Asset.save() : asset is required',
            reason : 'TypeError',
            level : 'error'
        };
    }

    if(account && account.isCollection()){
        throw {
            message : 'Asset.save() : account is a collection',
            reason : 'TypeError',
            level : 'error'
        };
    }

    if(account && account.data.id.toString() != asset.data.accountId.toString()){
        throw {
            message : 'Asset.save() : Account <-> Asset',
            reason : 'RelationshipError',
            level : 'error'
        };   
    }
    
    if(!req.data.asset || !req.data.asset.data || !req.data.asset.filename){
        throw {
            message : 'Asset.save() : asset data is missing',
            reason  : 'RequestError',
            level   : 'warning'
        }
    }

    var match, contentType, encoding, fileData, extension, kind;

    var assetData   = req.data.asset.data;
    var filename    = req.data.asset.filename;

    var DATA_PARSE  = /^data:([a-zA-Z\/]+);([a-z0-9]+),(.+)$/;

    if(helper.isString(assetData)){
        if(match = assetData.match(DATA_PARSE)){
            contentType = match[1];
            encoding    = match[2];
            fileData    = match[3];
            kind        = 'image';
        }
        else {
            contentType = 'text/plain';
            encoding    = 'utf8';
            fileData    = assetData.toString();
            kind        = 'binary';
        }
    }
    else {
        contentType = 'application/json';
        encoding    = 'utf8';
        fileData    = assetData;
        kind        = 'json';
    }

    
    asset.data.payload     = fileData;
    asset.data.contentType = contentType;
    asset.data.encoding    = encoding;
    asset.data.kind        = kind;
    asset.data.filename    = filename;
    
    asset.save();

    /* check on success */
    if (!asset.isSaved()) {
        throw {
            message : 'Asset.new() : fail to save',
            reason : asset.error,
            level : 'error'
        }
    }

    fileData = helper.isString(fileData)
        ? fileData
        : JSON.stringify(fileData);

    var uploadPath      = buildAssetPath(asset);

    var fileData        = new Buffer(fileData, encoding);
    var folderExists    = false;

    mkpath(uploadPath.folder, function(_err) {
        if(_err){
            throw {
                message : 'Asset.new() : Could not create path to file',
                reason : _err,
                level : 'error'
            }
        }
        folderExists = true;
    });

    deasync.loopWhile(function(){ return !folderExists; });

    try {
        fs.writeFileSync(uploadPath.path, fileData);
    } catch (_err) {
        throw{
            message : 'Asset.new() : Could not save the file',
            reason  : _err,
            level   : 'error'
        }  
    }

    if(req.data.asset.sets){

        asset.data.meta.dimensions   = req.data.asset.spriteDimensions || null;
        asset.data.meta.sets         = req.data.asset.sets;

        asset.save();

        /* check on success */
        if (!asset.isSaved()) {
            throw {
                message : 'Asset.new() : fail to save',
                reason : asset.error,
                level : 'error'
            }
        }
    }

    res.data.asset = asset;
    next && next();
    return asset;
}


/**
 *
 * @type {AssetController}
 */
module.exports = AssetController;

