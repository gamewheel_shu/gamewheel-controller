var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 * ChannelController
 * @constructor
 */
function ChannelController () {
    this.Channel = function () {
        return Gamewheel.Model.get('Channel');
    };
}

/**
 * evaluates the request parameter as a channel id
 *
 * @param   {Request}   req     expressJs Request Object
 * @param   {Response}  res     expressJs Response Object
 * @param   {Function}  next    trigger async next step
 * @param   {String}    id      channel id
 * 
 * @return  {Object}            request channel object
 */
ChannelController.prototype.identifier = function (req, res, next, id) {
    req.data.channel    = req.data.channel || {};
    req.data.channel.id = id;
    next && next();
    return req.data.channel;
}


/**
 * get a channel or channel collection
 *
 * @throws {TypeError}                          If CampaignCollection
 * @throws {TypeError}                          If ContentCollection
 * @throws {RelationshipError}                  If Campaign and it is a single Channel request
 *                                              under the condition the Channel <-> Campaign
 *                                              relationship does not match
 * @throws {RelationshipError}                  If Content and it is a single Channel request
 *                                              under the condition the Channel <-> Content
 *                                              relationship does not match
 * 
 * @param  {Request}                    req     expressJs Request Object
 * @param  {Response}                   res     expressJs Response Object
 * @param  {Function}                   next    next 
 * @return {Channel|ChannelCollection}          result of the request
 */
ChannelController.prototype.get = function(req, res, next) {

    var campaign    = res.data.campaign || null;
    var content     = res.data.content || null;
    var channel;

    if(campaign.isCollection()){
        throw {
            message : 'Channel.get() : CampaignCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    if(content.isCollection()){
        throw {
            message : 'Channel.get() : ContentCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    /**
     * this channel is requested by id
     * @param   {String | [ String ]}   req.data.channel
     */
    if(req.data.channel && req.data.channel.id) {
        
        channel = this.Channel().findById(req.data.channel.id);

        /**
         * single channel request
         */
        if(channel.isNotCollection()){
            
            /**
             * check if found
             */
            if (!channel.loadSuccess()) {
                throw {
                    message : 'Channel.get() : fail to load',
                    reason  : channel.error,
                    level   : 'error'
                };
            }

            /**
             * if there is a campaign the channel needs to
             * be bind to this campaign
             */
            if(campaign && campaign.data.id.toString() != channel.data.campaignId.toString() ){
                throw {
                    message : 'Channel.get() : Channel <-> Campaign',
                    reason  : 'RelationshipError',
                    level   : 'error'
                };
            }

            /**
             * if there is a campaign the channel needs to
             * be bind to this campaign
             */
            if(content && content.data.id.toString() != channel.data.contentId.toString() ){
                throw {
                    message : 'Channel.get() : Channel <-> Content',
                    reason  : 'RelationshipError',
                    level   : 'error'
                };
            }
        }

        /**
         * multiple channel request
         */
        else if(channel.isCollection()){

            /**
             * and campaign
             * check each channel if it is binded to this campaign
             */
            if(campaign || content){
                channel.reduce(function (_channel) {
                    
                    var fail = false;
                        fail = fail || (campaign    && campaign.data.id.toString()  != _channel.data.campaignId.toString());
                        fail = fail || (content     && content.data.id.toString()   != _channel.data.contentId.toString());
                    
                    return !fail
                        ? _channel
                        : null;
                });
            }
        }
    }

    /**
     * request by campaign
     * find all channels of binded to the campaign
     */
    else if(campaign){
        channel = this.Channel().find({
            campaignId : campaign.data.id
        });
    }

    /**
     * get all channels
     * @todo query arguments and options
     */
    else {
        channel = this.Channel().find({});
    }

    res.data.channel = channel;
    next && next();
    return channel;
};


/**
 * Create a new Channel
 *
 * @throws {TypeError}                          If CampaignCollection
 * @throws {TypeError}                          If ContentCollection
 * 
 * @param  {Request}                    req     expressJs Request Object
 * @param  {Response}                   res     expressJs Response Object
 * @param  {Function}                   next    next
 * 
 * @return {Channel}                    new Channel instance
 */
ChannelController.prototype.new = function(req, res, next){

    var campaign    = res.data.campaign || null;
    var content     = res.data.content || null;
    
    var channel;

    if(!campaign || campaign.isCollection()){
        throw {
            message : 'Channel.new() : CampaignCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    if(!content || content.isCollection()){
        throw {
            message : 'Channel.get() : ContentCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    channel = this.Channel().init({
        campaignId : campaign.data.id,
        contentId : content.data.id
    }).save();

    /**
     * if fail to save
     */
    if (!channel.saveSuccess() ) {
        throw {
            message : 'Channel.new() : fail to save',
            reason : channel.error,
            level   : 'error'
        }
    }

    res.data.channel = channel;
    next && next();
    return channel;
};


/**
 * clone a Channel
 *
 * @throws {TypeError}                          If there is not Channel to clone
 * @throws {TypeError}                          If Channel is a ChannelCollection
 * @throws {TypeError}                          If Campaign is a CampaignCollection
 * @throws {TypeError}                          If Content is a ContentCollection
 * 
 * @param  {Request}                    req     expressJs Request Object
 * @param  {Response}                   res     expressJs Response Object
 * @param  {Function}                   next    next
 * 
 * @return {Channel}                    new cloned Channel instance
 */
ChannelController.prototype.clone = function(req, res, next){

    var campaign    = res.data.campaign || null;
    var channel     = res.data.channel || null;
    var clone;

    if(!channel || channel.isCollection()){
        throw {
            message : 'channel.clone() : Channel',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    if(campaign.isCollection()){
        throw {
            message : 'Channel.clone() : CampaignCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    if(content.isCollection()){
        throw {
            message : 'Channel.clone() : ContentCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    clone = this.Channel().clone(channel);

    /**
     * if there is a campaign the channel needs to
     * be bind to this campaign
     */
    if(campaign){
        clone.data.campaignId = campaign.data.id;
    }

    /**
     * if there is a content the channel needs to
     * be bind to this content
     */
    if(content){
        clone.data.contentId = content.data.id;
    }

    clone.save();

    /**
     * if fail to save
     */
    if (!clone.saveSuccess() ) {
        throw {
            message : 'Channel.clone() : fail to save',
            reason : clone.error
        }
    }

    res.data.channel = clone;
    next && next();
    return clone;
};


/**
 * save the channel to resource endpoint
 * 
 * @throws {TypeError}                          If no Channel or no Collection
 * @throws {TypeError}                          If CampaignCollection
 * @throws {TypeError}                          If ContentCollection
 * @throws {RelationshipError}                  If Campaign and a single Channel request
 *                                              under the condition the Channel <-> Campaign
 *                                              relationship does not match
 * @throws {RelationshipError}                  If Content and it is a single Channel request
 *                                              under the condition the Channel <-> Content
 *                                              relationship does not match
 * 
 * @param  {Request}                    req     expressJs Request Object
 * @param  {Response}                   res     expressJs Response Object
 * @param  {Function}                   next    next
 * 
 * @return {Channel}                    new cloned Channel instance
 */
ChannelController.prototype.save = function(req, res, next){

    var campaign    = res.data.campaign || null;
    var channel     = res.data.channel || null;

    /* requirements */
    if(!channel){
        throw {message : 'channel save : missing channel'};
    }

    /**
     * data to save in the channels
     */
    if(!helper.isObject(req.data.channel)){
        throw {
            message : 'channel save : nothing to save',
            level : 'warning'
        };
    }

    channel.process(function(_channel){

        if(campaign && campaign.data.id.toString() != _channel.data.campaignId.toString() ){
            throw {message : 'permission deny : get Channel <-> Campaign' };
        }

        /* loops thru all data elements */
        Object.keys(req.data.membership).forEach(function(_key){
            if(helper.in(['_id', 'id'], _key)){ return; }

            _channel.data[_key] = req.data.membership[_key];
            _channel.save();
            
            /* check on success */
            if (!_channel.saveSuccess()) {
                throw {
                    message : 'Channel.save() : fail to save',
                    reason : _channel.error,
                    level : 'error'
                }
            }
        });
    });


    res.data.channel = channel;
    next && next();
    return channel;
};

ChannelController.prototype.setCampaign = function(req,res, next){
    
    var campaign = res.data.campaign || null;
    var channel = res.data.channel || null;

    if(!campaign || campaign.collection){
        throw {message : 'campaign addChannel : Campaign is missing or a CampaignCollection'};
    }

    if(!channel) {
        throw {message : 'campaign addChannel : Channel is missing'};
    }

    channel.process(function(_channel){
        _channel.data.campaignId = campaign.data.id;
        _channel.save();

        /* check on success */
            if (!_channel.saveSuccess()) {
                throw {
                    message : 'Channel.save() : fail to save',
                    reason : _channel.error,
                    level : 'error'
                }
            }
    });


    res.data.channel = channel;
    next && next();
    return channel;
};

/**
 *
 * @type {ChannelController}
 */
module.exports = ChannelController;