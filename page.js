var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 *
 * @constructor
 */
function PageController (){

    /** Models **/
    this.Page   = function () {
        return Gamewheel.Model.get('Page');
    };
}

PageController.prototype.identifier = function (req, res, next, id) {
    req.data.page    = req.data.page || {};
    req.data.page.id = id;
    next && next();
    return req.data.page;
};

PageController.prototype.get = function (req, res, next) {

    var page, error;
    var account = res.data.account || null;

    if(account && account.collection){
        throw {message : 'Page get : multible account request is not supported'};
    }

    if(req.data.page && req.data.page.id) {
        page = this.Page().findById(req.data.page.id);

        if(page.isNotCollection()){
            
            /* check if found */
            if (!page.loadSuccess()) {
                throw {
                    message : '',
                    reason : page.error,
                    level : 'error'
                };
            }

            if(account && !account.data.belongsTo(page)){
                throw {
                    message : 'permission deny : get Page <-> Account',
                    reason : 'access deny',
                    level : 'error'
                };
            }
        }

        else if(page.isCollection()){

            if(account){
                page.reduce(function (_page) {
                    return (account.data.belongsTo(_page))
                        ? _page
                        : null;
                });
            }
        }
    }

    else if(account){
        page = this.Page().findById(account.data.pageIds);
    }

    else {
        page = this.Page().find({});
    }

    res.data.page = page;
    next && next();
    return page;
};

PageController.prototype.new = function(req, res, next){

    var page;

    page = this.Page().init({}).save();
    
    /* check on success */
    if (!page.saveSuccess()) {
        throw {
            message : '',
            reason : page.error,
            level : error
        };
    }

    res.data.page = page;
    next && next();
    return page;
};


PageController.prototype.clone = function(req, res, next){

    var page = res.data.page;
    var clone;

    if(!page) {
        throw {message : 'Page clone : page is missing'};
    }
    if(!page.collection) {
        throw {message : 'Page clone : multible pages are not supported'};
    }

    clone = this.Page().clone(page).save();
    
    /* check on success */
    if (!clone.saveSuccess()) {
        throw {
            message : '',
            reason : clone.error,
            level : 'error'
        }
    }

    res.data.page = clone;
    next && next();
    return page;
};

PageController.prototype.save = function(req, res, next){

    var page = res.data.page || null;

    if(!page){
        throw {message :'page save : missing page'};
    }

    if(!helper.isObject(req.data.page)){
        throw {
            message : 'page save : nothing to save',
            level : 'warning'
        };
    }

    page.process(function(_page){
        
        /* loops thru all data elements */
        Object.keys(req.data.page).forEach(function(_key){
            if(helper.in(['_id', 'id'], _key)){ return; }

            _page.data[_key] = req.data.page[_key];
            _page.save();
            
            /* check on success */
            if (!_page.saveSuccess()) {
                throw {
                    message : '',
                    reason : _page.error,
                    level : 'error'
                };
            }
        });
    });

    res.data.page = page;
    next && next();
    return page;
};


PageController.prototype.template = function(req, res, next){

    var page, error;
    var account = res.data.account || null;

    if(account && account.isCollection()){
        throw {message : 'Page get : multible account request is not supported'};
    }

    page = this.Page().find({
        $bit : {
            'status' : { and : 0x1 },
            'permission.network'    : { and : 0x1 }
        }
    });

    if(account){
        accountPage = this.Page().findById(account.data.contentIds);
        accountPage.collection.forEach(function (_page) {
            if(_page.status & 0x1){
                page.collection.push(_page);
            }
        });
    }

    res.data.page = page;
    next && next();
    return page;
};

/**
 *
 * @type {AssetLibraryController}
 */
module.exports = exports = PageController;
