var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 *
 * @type {RoleController}
 */
module.exports = exports = RoleController;

/**
 *
 * @constructor
 */
function RoleController (){

    /** Models **/
    this.Role   = function () {
        return Gamewheel.Model.get('Role');
    };
}

RoleController.prototype.identifier = function (req, res, next, id) {
    req.data.role    = req.data.role || {};
    req.data.role.id = id;
    next && next();
    return req.data.role;
};


RoleController.prototype.get = function (req, res, next) {

    var role, error;
    var account = res.data.account || null;

    if(account && account.collection){
        throw {message : 'Role get : multiple account request is not supported'}
    }

    if(req.data.role && req.data.role.id) {
        role = this.Role().findById(req.data.role.id);

        if(role.isNotCollection()){
            
            /* check if found */
            if (!role.loadSuccess()) {
                throw {
                    message : '',
                    reason : role.error,
                    level : 'error'
                };
            }

            if(account && !account.data.belongsTo(role)){
                throw {message : 'permission deny : get Role <-> Account' };
            }
        }

        else if(role.isCollection()){

            if(account){
                role.reduce(function (_role) {
                    return (account.data.belongsTo(_role))
                        ? _role
                        : null;
                });
            }
        }
    }

    else if(account){
        role = this.Role().findById(account.data.roleIds);
    }

    else {
        role = this.Role().find();
    }

    res.data.role = role;
    next && next();
    return role;
};


RoleController.prototype.new = function (req, res, next) {

    var account = res.data.account || null;
    var role;

    if(account && role.isCollection()){
        throw {message : 'Role new : multiple account request is not supported'};
    }

    role = this.Role().init({
        name : 'Role Name'
    }).save();

    /** check on success **/
    if (!role.saveSuccess(role)) {
        throw {
            message : '',
            reason : role.error,
            level : 'error'
        };
    }

    res.data.role = role;
    next && next();
    return role;

};

RoleController.prototype.clone = function(req, res, next) {
    
    var role = res.data.role || null;
    var clone;

    if(!role){
        throw {message : 'Role clone : role is missing'};
    }

    if(role.isCollection()){
        throw {message : 'Role clone : multible roles are not supported'};
    }

    clone = this.Role().clone(role);
    clone.save();

    /** check on success **/
    if (!role.saveSuccess()) {
        throw {
            message : '',
            reason: role.error,
            level : 'error'
        };
    }    

    next && next();
    return clone;
};