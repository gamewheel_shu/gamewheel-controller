var Gamewheel               = require('../gamewheel');
var helper                  = Gamewheel.helper;
var config                  = Gamewheel.config;

/**
 *
 * @constructor
 */
function AssetLibraryController (){

    /** Models **/
    this.AssetLibrary   = function (){
        return Gamewheel.Model.get('AssetLibrary');
    };

    /** Models **/
    this.Asset   = function (){
        return Gamewheel.Model.get('Asset');
    };
}

AssetLibraryController.prototype.identifier = function (req, res, next, id) {
    req.data.assetLibrary       = req.data.assetLibrary || {};
    req.data.assetLibrary.id    = id;
    next && next();
    return req.data.assetLibrary;
};

AssetLibraryController.prototype.get = function (req, res, next) {

    var assetLibrary, error;
    var account = res.data.account || null;

    if(account && account.isCollection()){
        throw {
            message : 'AssetLibrary.get() : AccountCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    if(req.data.assetLibrary && req.data.assetLibrary.id) {

        assetLibrary = this.AssetLibrary().findById(req.data.assetLibrary.id);

        if(assetLibrary.isNotCollection()){
            
            /**
             * check if found
             */
            if (!assetLibrary.isLoaded()) {
                throw {
                    message : 'assetLibrary.get() : fail to load',
                    reason  : assetLibrary.error,
                    level   : 'error'
                };
            }

            /**
             * if there is a account the assetLibray needs to
             * be bind to this account
             */
            if(account && account.data.assetLibraryId.toString() != assetLibrary.data.id.toString() ){
                throw {
                    message : 'AssetLibrary.get() : AssetLibrary <-> Account',
                    reason  : 'RelationshipError',
                    level   : 'error'
                };
            }
        }

        else if(assetLibrary.isCollection()){

            if(account){
                assetLibrary.reduce(function (_assetLibrary) {
                    return (account.data.assetLibraryId.toString() != _assetLibrary.data.id.toString())
                        ? _assetLibrary
                        : null;
                });
            }
        }
    }

    else if(account){
        assetLibrary = this.AssetLibrary().findById(account.data.assetLibraryId);

        /**
         * check if found
         */
        if (!assetLibrary.isLoaded()) {
            throw {
                message : 'assetLibrary.get() : fail to load',
                reason  : assetLibrary.error,
                level   : 'error'
            };
        }

    }

    else {
        assetLibrary = this.AssetLibrary().find({});
    }

    res.data.assetLibrary = assetLibrary;
    next && next();
    return assetLibrary;
};

AssetLibraryController.prototype.new = function (req, res, next) {

    var assetLibrary;
    
    assetLibrary = this.AssetLibrary().init({}).save();

    /**
     * if fail to save
     */
    if (!assetLibrary.isSaved() ) {
        throw {
            message : 'AssetLibrary.new() : fail to save',
            reason : assetLibrary.error,
            level   : 'error'
        }
    }

    res.data.assetLibrary = assetLibrary;
    next && next();
    return assetLibrary;
};

AssetLibraryController.prototype.save = function(req, res, next){

    var assetLibrary = res.data.assetLibrary || null;

    if(!assetLibrary || assetLibrary.isCollection()){
        throw {
            message : 'AssetLibrary.save() : assetLibrary missing oris an AssetLibraryCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    if(!helper.isObject(req.data.assetLibrary)){
        throw {
            message : 'page save : nothing to save',
            level : 'warning'
        };
    }

    assetLibrary.perform(function(_assetLibrary){
        
        /* loops thru all data elements */
        Object.keys(req.data.assetLibrary).forEach(function(_key){
            if(helper.in(['_id', 'id'], _key)){ return; }

            _assetLibrary.data[_key] = req.data.page[_key];
            _assetLibrary.save();
            
                /* check on success */
                if (!_assetLibrary.isSaved()) {
                    throw {
                        message : 'AssetLibrary.save() : fail to save',
                        reason : _assetLibrary.error,
                        level : 'error'
                    }
                }
        });
    });

    res.data.assetLibrary = assetLibrary;
    next && next();
    return assetLibrary;
};

AssetLibraryController.prototype.add = function(req, res, next){

    var assetLibrary = res.data.assetLibrary || null;
    var asset        = res.data.asset || null;

    if(!assetLibrary || assetLibrary.isCollection()){
        throw {
            message : 'AssetLibrary.asset() : assetLibrary missing oris an AssetLibraryCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    assetLibrary.data.assetIds = assetLibrary.data.assetIds.concat(asset.getId(true));

    assetLibrary.save();

    if(!assetLibrary.isSaved()){
        throw {
            message : '',
            error : assetLibrary.error,
            level : 'error'
        };
    }

    res.data.asset = asset;
    next && next();
    return asset;
};

AssetLibraryController.prototype.asset = function(req, res, next){

    var assetLibrary = res.data.assetLibrary || null;
    var asset;

    if(!assetLibrary || assetLibrary.isCollection()){
        throw {
            message : 'AssetLibrary.asset() : assetLibrary missing oris an AssetLibraryCollection',
            reason  : 'TypeError',
            level   : 'error'
        };
    }

    asset = this.Asset().findById(assetLibrary.data.assetIds);

    res.data.asset = asset;
    next && next();
    return asset;
};

/**
 *
 * @type {AssetLibraryController}
 */
module.exports = exports = AssetLibraryController;