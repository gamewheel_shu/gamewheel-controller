var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 *
 * @type {AssetLibraryController}
 */
module.exports = exports = MembershipController;

/**
 *
 * @constructor
 */
function MembershipController (){

    /** Models **/
    this.Membership = function(){
        return Gamewheel.Model.get('Membership');
    };
}

MembershipController.prototype.identifier = function (req, res, next, id) {
    req.data.membership    = req.data.membership || {};
    req.data.membership.id = id;
    next && next();
    return req.data.membership;
};

MembershipController.prototype.get = function (req, res, next) {

    var user    = res.data.user     || null;
    var account = res.data.account  || null;
    var load    = false; // use 'find'
    var query = {};
    var membership;

    if(account && account.isCollection()){
        throw {message : 'membership <-> account : multiple account query is not supported '};
    }
    else if(account){
        query.accountId = account.data.id;
    }

    if(user && user.isCollection()){
        throw {message : 'membership <-> user : multiple user query is not supported '};
    }
    else if(user){
        query.userId = user.data.id;
    }

    if(req.data.membership && helper.isString(req.data.membership.id)) {
        load = true;
        query = { _id : req.data.membership.id };
    }
    else if(req.data.membership && helper.isArray(req.data.membership.id)) {
        query._id = {$in : req.data.membership.id};
    }

    membership = load
        ? this.Membership().load(query)
        : this.Membership().find(query);

    // if its a single membership
    if(load){
        
        /* check if found */
        if (membership.loadSuccess()) {
            throw {
                message : '',
                reason : membership.error,
                level : 'error'
            };
        }

        req.data.user       = {id : membership.data.userId};
        req.data.account    = {id : membership.data.accountId};

        // request account roles
        if(membership.data.accountRoleIds.length > 0){
            req.data.accountRole = {};
            req.data.accountRole.id = membership.data.accountRoleIds.map(function (_role) {
                return _role.toString();
            });
        }
        // request user role
        if(membership.data.userRoleId){
            req.data.userRole = {};
            req.data.userRole.id = membership.data.userRoleId.toString();
        }
    }

    // if its a membership collection and no account
    // request all account ids in the of the memberships
    else if(!account && user){
        req.data.account = {};
        req.data.account.id = membership.map(function(_membership){
            return _membership.data.accountId.toString();
        });
    }

    // if its a membership collection and no user
    // request all user ids in the of the memberships
    else if(!user && account){
        req.data.account    = {};
        req.data.account.id = membership.map(function(_membership){
            return _membership.data.userId.toString();
        });
    }

    res.data.membership = membership;

    next && next();
    return membership;
};

MembershipController.prototype.new = function (req, res, next) {

    var membership;
    var user    = res.data.user || null;
    var account = res.data.account || null;

    if(!account || account.isCollection()){
        throw {message : 'Membership : account missing'};
    }

    if(!user || user.isCollection()){
        throw {message : 'Membership : user missing'};
    }

    membership = this.Membership().init({
        user : user,
        account : account
    }).save();

    /* check on success */
    if (!membership.saveSuccess(membership)) {
        throw {
            message : '',
            reason : membership.error,
            level : 'error'
        };
    }

    res.data.membership = membership;
    next && next();
    return membership;

};

MembershipController.prototype.save = function(){
    var membership = res.data.membership || null;

    if(!membership){
        throw {message : 'membership save : missing membership'};
    }

    if(!helper.isObject(req.data.membership)){
        throw {
            message : 'membership save : nothing to save',
            level : 'warning'
        };
    }

    membership.process(function(_membership){
        
        /* loops thru all data elements */
        Object.keys(req.data.membership).forEach(function(_key){
            if(helper.in(['_id', 'id', 'user', 'userId', 'account', 'accountId'], _key)){ return; }

            _membership.data[_key] = req.data.membership[_key];
            _membership.save();
            
            /* check on success */
            if (!_membership.saveSuccess()) {
                throw {
                    message : '',
                    reason : _membership.error,
                    level : 'error'
                };
            }
        });
    });

    res.data.membership = membership;
    next && next();
    return membership;
};
