var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

var DataComponent           = require('./component/data');
var Session                 = require('../model/session');

/**
 *
 * @type {SessionController}
 */
module.exports = exports = SessionController;

/**
 *
 * @constructor
 */
function SessionController (){

    this.Session    = require('../../model/session');
}


/** account specific controller methods **/

/**
 * set/get the working account
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
SessionController.prototype.workingAccount = function (req, res, next) {

    var account = res.data.account || null;

    if(!account){
        throw { message : 'workingAccount : missing account'};
    }

    if(account.collection){
        throw { message : 'workingAccount : multible accounts are not supported'};
    }

    req.session.account = account.data.id;

    res.data.account = account;
    next && next();
    return account;
};

/**
 * sign in to the api and get a valid session token
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
SessionController.prototype.signIn = function (req, res, next) {

    var user = res.data.user || null;

    if(!user){
        throw {message : 'sign in : user is missing'};
    }

    if (!req.data.user || !req.data.user.password) {
        throw {
            result  : 'failure',
            status  : 403,
            message : 'Bad request - we expect {email:\'your@email.com\', password:\'your_password_123\'}'
        };
    }

    if (!user.data.authenticate(req.data.user.password)) {
        throw DataComponent.badPassword();
    }

    req.session.authenticate({
        ip          : req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        userId      : user.data.id,
        keep        : req.data.keep || false,
        environment : 'app',
        account     : user.data.account.toString()
    });
    
    // set account
    req.data.account = { id : user.data.account.toString() };

    res.data.user = user;
    next && next();
    return req.session;
};

/**
 * sign out of the api
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
SessionController.prototype.signOut = function (req, res, next) {

    /**
     * Create a blank new session
     * @type {Session}
     */
    req.session = new Session(null, config.api.sessionKey);
    delete res.data.user;
    delete res.data.account;
    
    next && next();
    return req.session;

    //TODO: store sessions to memcache and remove them when users sign out - otherwise sessions could be hi-jacked
    //TODO: by users spoofing IP and copying session token
};
