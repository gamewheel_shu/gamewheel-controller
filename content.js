var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 *
 * @constructor
 */
function ContentController () {

    /** Models **/
    this.Content = function (){
        return Gamewheel.Model.get('Content');
    };
}

ContentController.prototype.identifier = function (req, res, next, id) {
    req.data.content    = req.data.content || {};
    req.data.content.id = id;
    next && next();
    return req.data.content;
};


ContentController.prototype.get = function(req, res, next) {

    var content, error;
    var account = res.data.account || null;

    if(account && account.collection){
        throw {message : 'Content get : multible account request is not supported'};
    }

    if(req.data.content && req.data.content.id) {
        content = this.Content().findById(req.data.content.id);

        if(content.isNotCollection()){
            
            /**
             * check if found
             */
            if (!content.loadSuccess()) {
                throw {
                    message : 'Content.get() : fail to load',
                    reason  : content.error,
                    level   : 'error'
                };
            }

            if(account && !account.data.belongsTo(content)){
                throw {
                    message : 'permission deny : get Content <-> Account',
                    level : 'error'
                };
            }
        }

        else if(content.isCollection()){

            if(account){
                content.reduce(function (_content) {
                    return (account.data.belongsTo(_content))
                        ? _content
                        : null;
                });
            }
        }
    }

    else if(account){
        content = this.Content().findById(account.data.contentIds);
    }

    else {
        content = this.Content().find({});
    }

    res.data.content = content;
    next && next();
    return content;
};

ContentController.prototype.new = function(req, res, next){

    var content;

    var contentData    = req.data.content || {};
    content            = this.Content().init({}).save();

    /**
     * if fail to save
     */
    if (!content.saveSuccess() ) {
        throw {
            message : 'Content.new() : fail to save',
            reason : content.error,
            level   : 'error'
        }
    }

    res.data.content = content;
    next && next();
    return content;
};

ContentController.prototype.clone = function(req, res, next){

    var content = res.data.content;
    var clone;

    if(!content) {
        throw {message : 'Content clone : content is missing'};
    }
    if(content.isCollection()) {
        throw {message : 'Content clone : multible content is not supported'};
    }

    clone = this.Content().clone(content);
    clone.save();

    /**
     * if fail to save
     */
    if (!clone.saveSuccess() ) {
        throw {
            message : 'Content.clone() : fail to save',
            reason : clone.error
        }
    }

    res.data.content = clone;
    next && next();
    return content;
};

ContentController.prototype.save = function(req, res, next){

    var content = res.data.content || null;

    if(!content){
        throw {message :'content save : missing content'};
    }

    if(!helper.isObject(req.data.content)){
        throw {
            message : 'content save : nothing to save',
            level : 'warning'
        };
    }

    content.process(function(_content){
        
        /* loops thru all data elements */
        Object.keys(req.data.content).forEach(function(_key){
            if(helper.in(['_id', 'id'], _key)){ return; }

            _content.data[_key] = req.data.content[_key];
            _content.save();
            
            /* check on success */
            if (!_content.saveSuccess()) {
                throw {
                    message : 'Content.save() : fail to save',
                    reason : _content.error,
                    level : 'error'
                }
            }
        });
    });

    res.data.content = content;
    next && next();
    return content;
};

ContentController.prototype.template = function(req, res, next){

    var content, error;
    var account = res.data.account || null;
    var accountContent;
    if(account && account.collection){
        throw {message : 'Content get : multible account request is not supported'};
    }

    content = this.Content().find({
        $bit : {
            'status' : { and : 0x1 },
            'permission.network'    : { and : 0x1 }
        }
    });

    if(account){
        accountContent = this.Content().findById(account.data.contentIds);
        accountContent.collection.forEach(function (_content) {
            if(_content.status & 0x1){
                content.collection.push(_content);
            }
        });
    }

    res.data.content = content;
    next && next();
    return content;
};


/**
 *
 * @type {ChannelController}
 */
module.exports = ContentController;