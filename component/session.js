var _ = require('lodash');

function SessionComponent() {}

SessionComponent.invalid = function(req) {  

    if (!req.data.session && !req.headers['x-gamewheel']) {
        return {
            result: 'failure',
            status: 8,
            message: 'Are you logged in? We expect session data {session:\'your_session_data_here\'}'
        };
    }

    if (!req.session.authenticated(req.headers['x-forwarded-for'] || req.connection.remoteAddress)) {
        return {
            result: 'failure',
            status: 2,
            message: req.session.failReason || 'Your session did not authenticate'
        };
    }

    return false;
};

module.exports = SessionComponent;
