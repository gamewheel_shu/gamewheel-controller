var _ = require('lodash');

function DataComponent() {}

DataComponent.getSchemaProperties = function(model) {

    var paths = JSON.parse(JSON.stringify(model.schema.paths));

    delete paths.createdAt;
    delete paths.updatedAt;
    delete paths._id;
    delete paths.__v;

    return paths;
};

DataComponent.getModelObjectString = function(model, operationType) {

    var paths = DataComponent.getSchemaProperties(model);

    var objectMessage = {};

    if (operationType != 'create') {
        objectMessage['id'] = {required : true}
    }

    if (operationType == 'delete') {
        return JSON.stringify(objectMessage);
    }

    for (var path in paths) {
        if (paths.hasOwnProperty(path)) {
            if (operationType == 'create') {
                objectMessage[path] = {required : paths[path].isRequired, type : paths[path].instance}
            } else {
                objectMessage[path] = {type: paths[path].instance}
            }
        }
    }

    return JSON.stringify(objectMessage);
};

DataComponent.dataError = function(error) {

    if (error && error.name && error.name == 'ValidationError') {

        var errors = error.errors;

        var messages = [];

        for (var subError in errors) {
            if (errors[subError] instanceof Error) {
                messages.push(subError + ': ' + errors[subError].message);
            }
        }

        return {
            result:'failure',
            status:6,
            message:'Validations failed',
            error: error.message || "An unknown error occurred",
            validationFailures: messages
        };

    } else {

        var message = 'There was a server problem - please try again later';

        if (typeof error.message != 'undefined') {
            message = error.message;
        }

        return {
            result:'failure',
            status:3,
            message: message,
            error: error.message || "An unknown error occurred"
        };
    }
};

DataComponent.modelMissing = function(req, res, Model, message, example) {
    if (typeof req.data[Model.modelName.toLowerCase()] == 'undefined') {
        res.data = _.extend(
            res.data,
            {
                result: 'failure',
                status: 10,
                message: message,
                example: example
            }
        );

        res.status(400);
        return true
    }
    
    return false;  
};

DataComponent.idMissing = function(req, res, Model, message) {

    if (req.data[Model.modelName.toLowerCase()] && (typeof req.data[Model.modelName.toLowerCase()].id == 'undefined')) {
        res.data = _.extend(
            res.data,
            {
                result:'failure',
                status:14,
                message:message
            }
        );

        res.status(400);
        return true;
    }
   
    return false;
};

/**
 * Returns an error object if no data was found or false
 * @param model
 * @param customMessage
 * @returns {*}
 */
DataComponent.noneFound = function(model, customMessage) {
    if (!model.synced) {
        return {
            result: 'failure',
            status: 4,
            message: customMessage || 'No data could be found'
        };
    }
    return false;
};

/**
 * Returns a bad password response
 * @returns {{result: string, status: number, message: string}}
 */
DataComponent.badPassword = function() {
    return {
        result: 'failure',
        status: 7,
        message: 'The password is incorrect'
    };
};

/**
 *
 * @param model Model
 * @param customMessage
 * @returns {*}
 */
DataComponent.alreadyExists = function(model, customMessage) {
    if (model.synced) {
        var message = customMessage || 'The data already exists';
        
        return {
            result:'failure',
            status:5,
            message: message
        };
    }
    return false;
};

/**
 * Returns a happy update object or false if update failed
 * @param model Model
 * @param customMessage
 */
DataComponent.updateSuccess = function(model, customMessage) {

    if (model.synced) {
        return {
            result:'success',
            status:1,
            message: customMessage|| "Updated :)"
        };
    }

    return false;
};

/**
 * Returns true if model is saved
 * @param model Model
 */
DataComponent.saveSuccess = function(model) {

    if (model.synced) {
        return {
            result:'success',
            status:1,
            message:'Super :)'
        };
    }

    return false;
};

DataComponent.noData = function(data, customMessage) {

    if (!data) {
        return {
            result:'failure',
            status:9,
            message:customMessage || 'No data'
        };
    }

    return false;
};

DataComponent.deleteSuccess = function(res, affected) {
    res.data = _.extend(
        res.data,
        {
            result:'success',
            status:1,
            message:'Its gone! :)'
        }
    );

    res.data = _.extend(
        res.data,
        affected
    );

    res.status(200);
};

DataComponent.found = function(res, results) {
	res.data = _.extend(
		res.data,
		{
			result:'success',
			status:1,
			message:'Found :)'
		}
	);

	res.data = _.extend(
		res.data,
		results
	);

	res.status(200);
};

module.exports = DataComponent;