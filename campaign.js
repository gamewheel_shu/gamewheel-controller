var Gamewheel           = require('../gamewheel');
var helper              = Gamewheel.helper;
var config              = Gamewheel.config;

/**
 *
 * @constructor
 */
function CampaignController (){

    /** Models **/
    this.Campaign   = function () {
        return Gamewheel.Model.get('Campaign');
    };
}

CampaignController.prototype.identifier = function (req, res, next, id) {
    req.data.campaign    = req.data.campaign || {};
    req.data.campaign.id = id;
    next && next();
    return req.data.campaign;
}

/**
 *
 * @param req
 * @param res
 * @param next
 */
CampaignController.prototype.get = function (req, res, next) {

    var campaign, error;
    var account = res.data.account || null;
    var channel = res.data.channel || null;
    var collection = [];

    if(account && account.collection){
        throw {message : 'Campaign get : multiple account request is not supported'}
    }

    if(req.data.campaign && req.data.campaign.id){

        campaign = this.Campaign().findById(req.data.campaign.id);

        if(campaign.isNotCollection()){
            
            /* check if found */
            if (campaign.loadSuccess()) {
                throw DataComponent.dataError(error);
            }

            if(account && !account.data.belongsTo(campaign) ){
                throw {message : 'permission deny : get Campaign <-> Account' };
            }
        }

        else if(campaign.isCollection()){

            if(account){
                campaign.reduce(function (_campaign) {
                    return (account.data.belongsTo(_campaign))
                        ? _campaign
                        : null;
                });
            }
        }
    }

    else if(channel){
        campaign = this.Campaign().findById(channel.getId(true));
        
        if(account){
            campaign.reduce(function(_campaign){
                return account.belongsTo(_campaign)
                    ? _campaign
                    : null;
            });
        }
    }

    else if(account){
        campaign = this.Campaign().findById(account.data.campaignIds);
    }

    else {
        campaign = this.Campaign().find({});
    }

    res.data.campaign = campaign;
    next && next();
    return campaign;
};

CampaignController.prototype.new = function (req, res, next) {

    var campaign;
    var campaignData    = req.data.campaign || {};

    campaignData.status = this.Campaign().STATUS_DISABLED;
    campaign            = this.Campaign().init(campaignData).save();

    /** check on success **/
    if (!campaign.saveSuccess()) {
        throw {
            message : 'Campaign.new() : fail to save',
            reason : campaign.error,
            level   : 'error'
        };
    }

    res.data.campaign = campaign;
    next && next();
    return campaign;
};

CampaignController.prototype.clone = function (req, res, next) {

    var campaign = res.data.campaign || null;
    var clone;

    if(!campaign || campaign.collection){
        throw {message : 'Campaign clone : missing Campaign to clone'};
    }

    clone               = this.Campaign().clone(campaign);
    clone.data.name     = clone.data.name + '(COPY)';
    clone.data.status   = this.Campaign().STATUS_DISABLED;
    clone.save();

    /** check on success **/
    if (!clone.saveSuccess()) {
        throw {
            message : 'Campaign.clone() : fail to save clone',
            reason : clone.error,
            level   : 'error'
        };
    }

    res.data.campaign = clone;
    next && next();
    return campaign;
};

CampaignController.prototype.save = function(req, res, next){

    var campaign = res.data.campaign || null;

    if(!campaign){
        throw {message :'campaign save : missing campaign'};
    }

    if(!helper.isObject(req.data.campaign)){
        throw {
            message : 'campaign save : nothing to save',
            level : 'warning'
        };
    }

    campaign.process(function(_campaign){
        
        /* loops thru all data elements */
        Object.keys(req.data.campaign).forEach(function(_key){
            if(helper.in(['_id', 'id'], _key)){ return; }

            _campaign.data[_key] = req.data.campaign[_key];
            _campaign.save();
            
            /* check on success */
            if (!_campaign.saveSuccess()) {
                throw {
                    message : 'AssetLibrary.save() : fail to save',
                    reason : _campaign.error,
                    level : 'error'
                };
            }
        });
    });

    res.data.campaign = campaign;
    next && next();
    return campaign;
};

CampaignController.prototype.template = function(req, res, next){

    var campaign, error;
    var account = res.data.account || null;
    var accountCampaign;
    
    if(account && account.collection){
        throw {message : 'Content get : multible account request is not supported'};
    }

    campaign = this.Campaign().find({
        $bit : {
            'status' : { and : 0x1 },
            'permission.network'    : { and : 0x1 }
        }
    });

    if(account){
        accountCampaign = this.Campaign().findById(account.data.campaignIds);
        accountCampaign.collection.forEach(function (_campaign) {
            if(_campaign.status & 0x1){
                campaign.collection.push(_campaign);
            }
        });
    }

    res.data.campaign = campaign;
    next && next();
    return campaign;
};

/**
 *
 * @type {UserController}
 */
module.exports = CampaignController;