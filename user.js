var _                   = require('lodash');
//var  mailer              = require('nodemailer');


var Gamewheel           = require('../gamewheel');
var config              = Gamewheel.config;
var helper              = Gamewheel.helper;


//TODO: what happens when a find or something fails when a server goes down or something?

// var transport           = mailer.createTransport(config.api.mailer);

/**
 *
 * @type {UserController}
 */
module.exports = exports = UserController;

/**
 *
 * @constructor
 */
function UserController () {

    this.User = function () {
        return Gamewheel.Model('User');
    };
}

UserController.prototype.identifier = function (req, res, next, id) {
    req.data.user    = req.data.role || {};
    req.data.user.id = id;
    next && next();
    return req.data.user;
};

UserController.prototype.get = function (req, res, next) {

    var user, error;

    // check if it is a single instance the request
    if(req.data.user && req.data.user.id){

        user = this.User().load({
            _id : req.data.user.id
        });

        /* check if found */
        if (!user.loadSuccess()) {
            throw {
                message : '',
                reason : user.error,
                level : 'error'
            };
        }
    }

    // a list of user
    else if(req.data.user && req.data.user.email){

        user = this.User().load({
            email : req.data.user.email
        });

         /* check if found */
        if (!user.loadSuccess()) {
            throw {
                message : '',
                reason : user.error,
                level : 'error'
            };
        }
    }

    // a list of user
    else if(req.data.user && req.data.user.authentication && req.data.user.authentication.confirmId){

        user = this.User().load({
            'authentication.confirmId' : req.data.user.authentication.confirmId
        });

         /* check if found */
        if (!user.loadSuccess()) {
            throw {
                message : '',
                reason : user.error,
                level : 'error'
            };
        }
    }


    if(!user){
        throw {message : 'User get : not found'};
    }

    res.data.user = user;
    next && next();
    return user;
};

UserController.prototype.new = function(req, res, next){

    var user, error;

    if(!req.data.user || !req.data.user.email){
        throw {message : 'user new : missing email'}; }

    if(!req.data.user || !req.data.user.password) {
        throw {message : 'user new : missing password'}; }

    if (error = DataComponent.alreadyExists( this.User().load({ email: req.data.user.email }), 'The user already exists' )) {
        throw error; }

    user = this.User().init({
        email : req.data.user.email,
        password : req.data.user.password
    }).save();

    /* check on success */
    if ( !user.saveSuccess() ) {
        throw {
            message : '',
            reason : user.error,
            level : error
        };
    }
    
    res.data.user = user;
    next && next();
    return user;
};

UserController.prototype.save = function(req, res, next) {

    var user = res.data.user || null;

    /* requirements */
    if(!user){
        throw {message : 'user save : missing user'};
    }

    if(!helper.isObject(req.data.user)){
        throw {
            message : 'user save : nothing to save',
            level : 'warning'
        };
    }

    user.process(function(_user){

        /* loops thru all data elements */
        Object.keys(req.data.membership).forEach(function(_key){
            if(helper.in(['_id', 'id'], _key)){ return; }

            _user.data[_key] = req.data.membership[_key];
            _user.save();
            
            /* check on success */
            if (!_user.saveSuccess()) {
                throw {
                    message : '',
                    reason : _user.error,
                    level : 'error'
                };
            }
        });
    });

    
    res.data.user = user;
    next && next();
    return user;

};



// UserController.prototype.getOrInvite = function (req, res, next) {

//     var user;

//     try{
//         user = this.get(req, res);
//     } catch (_e) {
//         user = this.invite(req, res);
//     }

//     res.data.user = user;
//     next && next();
//     return user;
// };

// UserController.prototype.invite = function (req, res, next) {

//     var user;

//     if(!req.data.user || req.data.user.email){
//         throw {message : 'User : invitation without email'};
//     }

//     req.data.user.password = helper.randomString(8);
//     req.data.user.status = this.User().STATUS_INVITED;
//     user = this.new (req, res);

//     /* check on success */
//     if ( !DataComponent.saveSuccess(user) ) {
//         throw DataComponent.dataError( user.error );
//     }

//     /**
//      * @todo: send invitation
//      */

//     res.data.user = user;
//     next && next();
//     return user;
// };



// UserController.prototype.clone = function (req, res, next) {
//     throw { message : 'User can not be cloned' };
// };



// UserController.prototype.delete = function (req, res, next) {
//     throw { message : 'User can not be deleted' };
// };

/** user specific controller methods **/

// UserController.prototype.reset = function(req, res, next) {

//     var user;

//     if (!req.data.user || !req.data.user.email) {
//         throw {
//             result:'failure',
//             status:0,
//             message:'Bad request - we need an email address ex. {email : \'your@email.com\'}'
//         };
//     }

//     user = this.get(req, res);
//     user.data.reset();
//     user.save();

//     if (!DataComponent.saveSuccess(user)) {
//         throw DataComponent.dataError();
//     }

//     //transport.sendMail(
//     //    {
//     //        from : '"Gamewheel API" <info@gamewheel.com>',
//     //        to : req.data.email,
//     //        subject : 'Password reset link',
//     //        text : config.app.url + '/reset/' + user.data.authentication.resetLinkId,
//     //        html : config.app.url + '/reset/' + user.data.authentication.resetLinkId
//     //    },
//     //    function(error, info) {
//     //
//     //        if (error) {
//     //
//     //            throw {
//     //                result:'failure',
//     //                status:10,
//     //                message:'There was a server problem during email, please try again later',
//     //                error: error.message
//     //            };
//     //        }
//     //
//     //        res.data = helper.extend(
//     //            res.data,
//     //            {
//     //                result:'success',
//     //                status:1,
//     //                message:'The password reset email has been sent',
//     //                info : JSON.stringify(info)
//     //            }
//     //        );
//     //        res.status(200);
//     //        return next();
//     //    }
//     //);

//     res.data.user = user;
//     next && next();
//     return user;
// };

// UserController.prototype.confirm = function (req, res, next) {

//     var user;

//     if (!req.data.user.authentication.confirmId) {
//         throw {
//             result:'failure',
//             status: 0,
//             message:'Bad request - we need a proper reset link (ex. http://api.gamewheel.com/resetPassword/your_reset_link_id)'
//         };
//     }

//     if (!req.data.user.password) {
//         throw {
//             result:'failure',
//             status:4,
//             message:'Bad request - you need to provide a new password : {password : \'your_new_password\'}'
//         };
//     }

//     user = this.get(req, res);

//     user.data.password                      = req.data.user.password;
//     user.data.authentication.resetLinkId    = null;
//     user.save();

//     if (!DataComponent.saveSuccess(user)) {
//         throw DataComponent.dataError();
//     }

//     res.data.user = user;
//     next && next();
//     return user;
// };

UserController.prototype.defaultAccount = function (req, res, next) {

    var account = res.data.account || null;
    var user    = res.data.user || null;

    /* requirements */
    if(!user ){
        throw { message : 'user : missing'};
    }

    if(!account){
        req.data.account = { id : user.data.account };
    } else {
        user.data.account = account.data.id;
        user.save();

        /* check on success */
        if (!user.saveSuccess()) {
            throw {
                message : '',
                reason : user.error,
                level : 'error'
            };
        }
    }

    next && next();
    return user;
};

// UserController.prototype.getAccounts = function (req, res, next) {

//     var account, user;

//     /* requirements */
//     if(!req.data.user ||  !req.data.user.id ){
//         throw { message : 'no user id provided'};
//     }

//     user = Gamewheel.Model('User').load({
//         _id : req.data.user.id
//     });

//     /* check if found */
//     if (DataComponent.noneFound(user)) {
//         throw DataComponent.dataError(user.error);
//     }

//     if(req.data.account && req.data.account.id){
//         account = user.data.accounts.reduce(function (_account, _userAccount) {
//             if(_account) { return _account; }

//             return _userAccount.data.id == req.data.account.id
//                 ? _userAccount.toData()
//                 : _account;

//         }, null);

//         /* check if found */
//         if (!account) {
//             throw {
//                 result: 'failure',
//                 status: 4,
//                 message: 'No data could be found'
//             };
//         }
//     }
//     else {
//         account = user.data.accounts.map(function (_account) {
//             return _account.toData();
//         });
//     }

//     /* create result */
//     res = helper.express.response(res);
//     res.data = helper.extend(res.data, {
//         result  : 'success',
//         status  : 1,
//         message : 'get user account',
//         account : account
//     });

//     res.status(200);
//     next && next();
//     return account;
// };

UserController.prototype.changePassword = function(req, res, next){
    var user = res.data.user || null;

    if(!user){
        throw {message : 'user change password : missing user'};
    }

    if (!req.data.user.password) {
        throw { message:'Bad request - you need to provide a new password : {password : \'your_new_password\'}' };
    }

    user.data.password                      = req.data.user.password;
    user.data.authentication.resetLinkId    = null;
    user.save();

    if (!user.saveSuccess()) {
        throw {
            message : '',
            reason : user.error,
            level : 'error'
        }
    }

    res.data.user = user;
    next && next();
    return user;
};